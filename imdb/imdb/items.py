# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import re

from scrapy import Field, Item
from scrapy.loader.processors import MapCompose, TakeFirst


def rm_end_points(value):
    if value:
        return re.sub(r'(\.+)$', '', value)


def strip(value):
    if value:
        return value.strip()


class TitleItem(Item):
    genre = Field(serializer=str,
                  input_processor=MapCompose(strip),
                  output_processor=TakeFirst())
    rank = Field(serializer=int,
                 input_processor=MapCompose(rm_end_points),
                 output_processor=TakeFirst())
    name = Field(serializer=str, output_processor=TakeFirst())
    rating = Field(serializer=float, output_processor=TakeFirst())
