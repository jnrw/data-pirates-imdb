# -*- coding: utf-8 -*-
from scrapy import Request, Spider
from scrapy.loader import ItemLoader

from ..items import TitleItem


class TopGenresSpider(Spider):
    """
    Spider for collect the first 500 titles of each genre from the imdb site.
    The titles are sorted by rating.
    """
    name = 'top_genres'
    allowed_domains = ['imdb.com']
    TITLES_LIMIT = 250

    def start_requests(self):
        url_genres_list = 'http://www.imdb.com/chart/top'
        yield Request(url=url_genres_list, callback=self.parse_genres_list)

    def parse_genres_list(self, response):
        """Gets the url and name of each genre, then dispatch a request."""
        query_genres_tag_a = 'ul.quicklinks li a'
        query_genres_name = 'a::text'
        query_genres_url = 'a::attr(href)'

        genres_tag_a = response.css(query_genres_tag_a)

        for tag in genres_tag_a:
            genre_name = tag.css(query_genres_name).extract_first()
            genre_url = tag.css(query_genres_url).extract_first()
            final_url = response.urljoin(genre_url)
            request = Request(url=final_url, callback=self.parse_titles)
            request.meta['genre'] = genre_name
            yield request

    def parse_titles(self, response):
        """Gets the rank, name and rating of each title, then follow to the
        next page.
        """
        query_titles_content = 'div.lister-item-content'
        query_title_rank = 'h3.lister-item-header > span.lister-item-index::text'
        query_title_name = 'h3.lister-item-header > a::text'
        query_title_rating = 'div.ratings-imdb-rating::attr(data-value)'
        query_curr_last_item = 'span.lister-current-last-item::text'
        query_next_page = 'a.lister-page-next::attr(href)'

        genre = response.meta.get('genre', '')

        sel_titles_div = response.css(query_titles_content)

        for sel_title_div in sel_titles_div:
            item = ItemLoader(item=TitleItem(), selector=sel_title_div)
            item.add_value('genre', genre)
            item.add_css('name', query_title_name)
            item.add_css('rank', query_title_rank)
            item.add_css('rating', query_title_rating)
            yield item.load_item()

        curr_last_item = response.css(query_curr_last_item).extract_first()
        next_page = response.css(query_next_page).extract_first()

        if next_page and curr_last_item:
            curr_last_item = int(curr_last_item)
            if curr_last_item < self.TITLES_LIMIT:  # check limit of titles
                final_url = response.urljoin(next_page)
                request = Request(url=final_url, callback=self.parse_titles)
                request.meta['genre'] = genre
                yield request
