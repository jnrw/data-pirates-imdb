# Data Pirates

Collects imformation from imdb site.

## Requirements

  * Python 3
  * Scrapy

## Usage

```sh
cd imdb
scrapy crawl top_genres -t jsonlines -o ~/titles.jsonl
```
